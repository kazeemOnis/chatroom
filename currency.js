var exchange = 360;

function roundTwoDec(no) {
    return Math.round(no * 100) / 100;
}

exports.nairaToDollars = (naira) => {
    return roundTwoDec(naira / exchange);
}

exports.dollarsToNaira = (dollars) => {
    return roundTwoDec(dollars * exchange);
}