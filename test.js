var currency = require('./currency');
var http = require('http');
var fs = require('fs');

http.createServer((req, res) => {
  if (req.url == '/') {
    getTitles(res);
  }
}).listen(3000);

function getTitles(res) {
  fs.readFile('./titles.json', (err, data) => {
    if (err) {
      handleError(err, res);
    } else {
      getTemplates(JSON.parse(data.toString()), res);
    }
  });
}

function getTemplates(titles, res) {
  fs.readFile('./template.html', (err, data) => {
    if (err) {
      handleError(err, res);
    } else {
      formatHtml(titles, data.toString(), res);
    }
  });
}

function formatHtml(titles, temp, res) {
  var html = temp.replace("%", titles.join('</li><li>'));
  res.writeHead(200, {
    'Content-Type': 'text/html'
  });
  res.end(html);
}


function handleError(err, res) {
  console.log(err);
  res.end(err);
}

console.log("50 dollars to Naira is")
console.log(currency.dollarsToNaira(50));

console.log("150,000 Naira to dollars is");
console.log(currency.nairaToDollars(150000));